import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick.Window   2.15

import xLogger         1.0 as L

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Mpl      1.0 as X

X.Application {

    id: window;

    width: 300;
    height: 400;

    visible: true;

    X.FigureBridge {
        id: _mpl_bridge;
        objectName: "bridge";
    }

    Page {

        anchors.fill: parent;

        X.FigureCanvas {
            id: _mpl_canvas;

            objectName: "canvas";
            dpi_ratio: Screen.devicePixelRatio

            anchors.fill: parent

            onNumberChanged: console.info('Figure number changed to:', _mpl_canvas.dpi_ratio);
        }
    }

    footer: ToolBar {
        RowLayout {
            Button {
                text: qsTr("home")
                onClicked: {
                    _mpl_bridge.home();
                }
            }

            Button {
                text: qsTr("back")
                onClicked: {
                    _mpl_bridge.back();
                }
            }

            Button {
                text: qsTr("forward")
                onClicked: {
                    _mpl_bridge.forward();
                }
            }

            ToolSeparator{}

            Button {
                id: pan
                text: qsTr("pan")
                checkable: true
                onClicked: {
                    if (zoom.checked) {
                        zoom.checked = false;
                    }
                    _mpl_bridge.pan();
                }
            }

            Button {
                id: zoom
                text: qsTr("zoom")
                checkable: true
                onClicked: {
                    if (pan.checked) {
                        // toggle pan off
                        pan.checked = false;
                    }
                    _mpl_bridge.zoom();
                }
            }
            ToolSeparator {}
            Label {
                id: location
                text: _mpl_bridge.coordinates
            }
        }
    }

    Component.onCompleted: {
        _mpl_bridge.updateWithCanvas(_mpl_canvas);
    }
}
