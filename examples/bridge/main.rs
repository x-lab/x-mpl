use qmetaobject::prelude::*;

use x_logger;
use x_gui;
use x_quick;
use x_mpl;

qrc!(register_resources, "/" {
    "examples/bridge/main.qml",
});

fn main() {

    register_resources();

    x_logger::init();
     x_quick::init();
       x_mpl::init();

let command = r#"
import numpy as np

from matplotlib_backend_qtquick.backend_qtquickagg import (FigureCanvasQtQuickAgg)
from matplotlib_backend_qtquick.backend_qtquick import NavigationToolbar2QtQuick
from matplotlib_backend_qtquick.qt_compat import QtGui, QtQml, QtCore, QtQuick, QtWidgets


class FigureBridge(QtCore.QObject):
    """ A bridge class to interact with the plot in python
    """
    coordinatesChanged = QtCore.Signal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        # The figure and toolbar
        self.figure = None
        self.toolbar = None

        # this is used to display the coordinates of the mouse in the window
        self._coordinates = ""

    @QtCore.Slot(FigureCanvasQtQuickAgg)
    def updateWithCanvas(self, canvas):
        """ initialize with the canvas for the figure
        """
        self.figure = canvas.figure
        self.toolbar = NavigationToolbar2QtQuick(canvas=canvas)

        # make a small plot
        self.axes = self.figure.add_subplot(111)
        self.axes.grid(True)

        x = np.linspace(0, 2*np.pi, 100)
        y = np.sin(x)

        self.axes.plot(x, y)
        canvas.draw_idle()

        # connect for displaying the coordinates
        self.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)

    # define the coordinates property
    # (I have had problems using the @QtCore.Property directy in the past)
    def getCoordinates(self):
        return self._coordinates

    def setCoordinates(self, coordinates):
        self._coordinates = coordinates
        self.coordinatesChanged.emit(self._coordinates)

    coordinates = QtCore.Property(str, getCoordinates, setCoordinates,
                                  notify=coordinatesChanged)

    # The toolbar commands
    @QtCore.Slot()
    def pan(self, *args):
        """Activate the pan tool."""
        self.toolbar.pan(*args)

    @QtCore.Slot()
    def zoom(self, *args):
        """activate zoom tool."""
        self.toolbar.zoom(*args)

    @QtCore.Slot()
    def home(self, *args):
        self.toolbar.home(*args)

    @QtCore.Slot()
    def back(self, *args):
        self.toolbar.back(*args)

    @QtCore.Slot()
    def forward(self, *args):
        self.toolbar.forward(*args)

    def on_motion(self, event):
        """
        Update the coordinates on the display
        """
        if event.inaxes == self.axes:
            self.coordinates = f"({event.xdata:.2f}, {event.ydata:.2f})"


QtQml.qmlRegisterType(FigureBridge          , "xQuick.Mpl", 1, 0, "FigureBridge")
QtQml.qmlRegisterType(FigureCanvasQtQuickAgg, "xQuick.Mpl", 1, 0, "FigureCanvas")
"#;

    x_mpl::interpret(command);

    let mut engine = x_gui::xApplicationEngine::new_profiled();
    engine.set_application_name("x-mpl Test".into());
    engine.set_organisation_name("inria".into());
    engine.set_organisation_domain("fr".into());
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path("qrc:///src".into());
    engine.set_source("qrc:///examples/bridge/main.qml".into());
    engine.exec();
}
