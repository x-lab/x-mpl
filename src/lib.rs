#![recursion_limit="256"]

use cpp::cpp;
use qmetaobject::prelude::*;

cpp! {{
#include <QtDebug>
#include <QtCore>

#include <QtQml>
#include <QtQuick>

#include <dtkScript>
}}

qrc!(register_resources, "/" {
    "src/matplotlib_backend_qtquick/__init__.py",
    "src/matplotlib_backend_qtquick/backend_qtquick.py",
    "src/matplotlib_backend_qtquick/backend_qtquickagg.py",
    "src/matplotlib_backend_qtquick/qt_compat.py",
});

cpp! {{

void copyDirectory(QString from, QString to)
{
    QDirIterator it(from, QDirIterator::Subdirectories);

    while (it.hasNext()) {

        QString file_in = it.next();

        QFileInfo file_info = QFileInfo(file_in);

        QString file_out = file_in;
        file_out.replace(from, to);

        if(file_info.isFile()) {
            qDebug() << QFile::copy(file_in, file_out);
            qDebug() << file_in << "--->" << file_out;
        }

        if(file_info.isDir()) {
            QDir dir(file_out);
            if (!dir.exists())
                qDebug() << "mkpath" << dir.mkpath(".");
        }
    }
}

}}

pub fn init() {
    register_resources();

    let envrnmt = qttypes::QString::from(std::env::var("CONDA_PREFIX").unwrap_or_default());

    cpp!(unsafe [envrnmt as "QString"] {
        QDir dir(envrnmt);
        dir.cd("lib");
        dir.cd("python3.7");
        dir.cd("site-packages");

        if(!dir.exists("matplotlib_backend_qtquick"))
            dir.mkdir ("matplotlib_backend_qtquick");

        copyDirectory(":src/matplotlib_backend_qtquick", QString("%1/lib/python3.7/site-packages/matplotlib_backend_qtquick").arg(envrnmt));
    });
}

pub fn interpret(command: &str) {

    let source = qttypes::QString::from(command);

    cpp!(unsafe [source as "QString"] {
        int stat; dtkScriptInterpreterPython::instance()->interpret(QString(source), &stat);
    });
}
